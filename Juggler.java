import java.util.ArrayList;
import java.util.List;

public class Juggler {
	// hand to eye coordination, endurance, pizzazz
	private int h, e, p;
	private int jugglerID; // juggler name is 'J'+jugglerID
	public List<Integer> preferredCircuits = new ArrayList<Integer>();
	private int currentCircuitIndex=-1;

	public Juggler(String data) {
		String[] tokens = data.split("[ ,]+");
		jugglerID = Integer.valueOf(tokens[1].substring(1));
		h = Integer.valueOf(tokens[2].substring(2));
		e = Integer.valueOf(tokens[3].substring(2));
		p = Integer.valueOf(tokens[4].substring(2));
		for (int i = 5; i < tokens.length; i++) {
			preferredCircuits.add(Integer.valueOf(tokens[i].substring(1)));
		}
	}

	public int getH() {
		return h;
	}
	
	public int getID(){
		return jugglerID;
	}

	public Integer getNextPreferred() {
		if ((currentCircuitIndex+1)>=preferredCircuits.size())
			return null;
		currentCircuitIndex++;
		return preferredCircuits.get(currentCircuitIndex);
	}

	public int preferredCircuitSize() {
		return preferredCircuits.size();
	}

	public int getE() {
		return e;
	}

	public int getP() {
		return p;
	}
}
