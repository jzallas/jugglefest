import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class juggle_fest_main {

	final static String IN_FILE_NAME = "C:\\jugglefest.txt";
	final static String OUT_FILE_NAME = "C:\\output.txt";
	final static Charset ENCODING = StandardCharsets.UTF_8;
	private static List<Juggler> jugglers;
	private static List<Circuit> circuits;
	private static List<Juggler> rejects;
	private static int matchesPerCircuit;

	public static String printMatchValues(Juggler j) {
		String line = "";
		for (int i = 0; i < j.preferredCircuits.size(); i++) {
			line += (" C" + j.preferredCircuits.get(i) + ":" + dotProduct(j,
					circuits.get(j.preferredCircuits.get(i))));
		}
		return line;
	}

	public static void main(String[] args) throws IOException {
		jugglers = new ArrayList<Juggler>();
		circuits = new ArrayList<Circuit>();
		rejects = new ArrayList<Juggler>();
		readTextFile(IN_FILE_NAME);
		matchesPerCircuit = jugglers.size() / circuits.size();
		matchJugglersToCircuits();

		writeTextFile(OUT_FILE_NAME);
	}

	private static Juggler attemptToReplace(int circuitID, Juggler j) {

		// the match value for the new juggler
		int newMatch = dotProduct(j, circuits.get(circuitID));
		// the first match value from the existing circuit is the smallest until
		// updated
		int smallestMatch = dotProduct(circuits.get(circuitID).matches.get(0),
				circuits.get(circuitID));
		int smallestMatchIndex = 0;
		// cycle thru the remaining n-1 match values
		for (int i = 1; i < matchesPerCircuit; i++) {
			int currentMatch = dotProduct(
					circuits.get(circuitID).matches.get(i),
					circuits.get(circuitID));
			// if the currentMatch is smaller than the smallest, update the
			// smallest match's value and index
			if (currentMatch < smallestMatch) {
				smallestMatch = currentMatch;
				smallestMatchIndex = i;
			}
		}

		// after finding the smallest match, if the newest match can replace it,
		// then replace it and return the removed juggler
		if (newMatch > smallestMatch) {
			Juggler replacement = circuits.get(circuitID).matches
					.get(smallestMatchIndex);
			circuits.get(circuitID).matches.remove(smallestMatchIndex);
			circuits.get(circuitID).matches.add(j);
			return replacement;
		}
		return j;
	}

	private static void insertIntoCircuit(Integer circuitID, Juggler j) {
		try {

			// try to insert into circuit if fits
			if (circuits.get(circuitID).matches.size() < matchesPerCircuit) {
				circuits.get(circuitID).matches.add(j);
			}
			// if circuit is full but juggler deserves to be there
			else {
				// try to replace someone in circuit
				Juggler replacement = attemptToReplace(circuitID, j);
				// need to reinsert the juggler or the replacement into the next
				// preferred circuit
				insertIntoCircuit(replacement.getNextPreferred(), replacement);
			}
		} catch (Exception e) {
			// this juggler failed to be inserted into all of its preferred
			// circuits
			rejects.add(j);
		}
	}

	private static void findBestFitAndInsert(final Juggler j) {
		List<Circuit> clonedList = new ArrayList<Circuit>(circuits.size());
		for (Circuit c : circuits)
			clonedList.add(new Circuit(c));

		Collections.sort(clonedList, new Comparator<Circuit>() {

			@Override
			public int compare(Circuit arg0, Circuit arg1) {
				int startComparison = compare(dotProduct(j, arg1),
						dotProduct(j, arg0));
				return startComparison;
			}

			private int compare(int A, int B) {
				return A < B ? -1 : A > B ? 1 : 0;
			}
		});

		for (Circuit c : clonedList) {
			if (circuits.get(c.getID()).matches.size() == matchesPerCircuit) { // if
																				// the
																				// best
																				// possible
																				// circuit
																				// is
																				// full,
																				// check
																				// for
																				// replacement
				Juggler temp = attemptToReplace(c.getID(), j);
				if (temp != j) { // if replaced, reinsert the replacement
					insertIntoCircuit(temp.getNextPreferred(), temp);
					break;
				}
			} else
				// if circuit has room, just add this juggler to circuit
				circuits.get(c.getID()).matches.add(j);
		}
	}

	private static void matchJugglersToCircuits() {
		for (int i = 0; i < jugglers.size(); i++) {
			insertIntoCircuit(jugglers.get(i).getNextPreferred(),
					jugglers.get(i));
		}
		// deal with rejects
		while (!rejects.isEmpty()) {
			Juggler j = rejects.get(0);
			rejects.remove(0);
			findBestFitAndInsert(j);
		}
	}

	private static int dotProduct(Juggler j, Circuit c) {
		// dot product of juggler and circuit is
		// [x,y,z] and [q,r,t] = xq + yr + zt
		return j.getH() * c.getH() + j.getE() * c.getE() + j.getP() * c.getP();
	}

	public static void readTextFile(String inFileName) throws IOException {
		Path path = Paths.get(inFileName);
		try (Scanner scanner = new Scanner(path, ENCODING.name())) {
			while (scanner.hasNextLine()) {
				// process each line in some way
				String line = String.valueOf(scanner.nextLine());
				if (line.startsWith("C")) {
					Circuit c = new Circuit(line);
					circuits.add(c);
				} else if (line.startsWith("J")) {
					Juggler j = new Juggler(line);
					jugglers.add(j);
				}
			}
		}
	}

	private static void writeTextFile(String outFileName) throws IOException {
		Path path = Paths.get(outFileName);
		try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
			for (Circuit c : circuits) {
				String line = "";
				line += ("C" + c.getID() + " ");
				for (int i = 0; i < c.matches.size(); i++) {
					if (i != 0)
						line += (", ");
					line += ("J" + c.matches.get(i).getID());
					line += printMatchValues(c.matches.get(i));
				}
				System.out.println(line);
				writer.write(line);
				writer.newLine();
			}
		}
	}
}
