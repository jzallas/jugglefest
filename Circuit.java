import java.util.ArrayList;
import java.util.List;


public class Circuit {
	//hand to eye coordination, endurance, pizzazz
	private int h, e, p;
	private int circuitID; //circuit name is 'C'+circuitID
	public List<Juggler> matches;
	
	public Circuit(String data){
		matches = new ArrayList<Juggler>();
		String[] tokens = data.split("[ ]+");
		circuitID = Integer.valueOf(tokens[1].substring(1));
		h = Integer.valueOf(tokens[2].substring(2));
		e = Integer.valueOf(tokens[3].substring(2));
		p = Integer.valueOf(tokens[4].substring(2));
	}
	
	public Circuit(Circuit c){
		circuitID = c.getID();
		h = c.getH();
		e = c.getE();
		p = c.getP();
	}
	
	public int getID(){
		return circuitID;
	}
	
	
	public int getH()
	{
		return h;
	}
	
	public int getE()
	{
		return e;
	}
	
	public int getP()
	{
		return p;
	}
}
